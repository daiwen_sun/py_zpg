from requests import get
from bs4 import BeautifulSoup
import json
import pandas as pd
import fetch

p_pagenum = 1
p_period = 24
area = "edinburgh"
# url = "https://espc.com/house-prices/edinburgh-and-lothians?ps=50&p=" + str(p_pagenum) + "&sold=" + str(p_period)

def espc_total_pages(json_data):
    return json_data['total']

def page_calc_espc(url):
    target_index = -1
    response = get(url)
    html_soup = BeautifulSoup(response.text, 'html.parser')
    # containers = html_soup.find('script', type='application/json')
    containers = html_soup.find_all('script')
    for i in range(len(containers)):
        if(str(containers[i]).find('postcode')) != -1:
            target_index = i
            #TODO: Need error handling here.
    # print(containers[target_index])
    # Remove first few characters for JSON
    head_trim = str(containers[target_index]).find('{')
    tail_trim = str(containers[target_index]).rfind(';')
    target_string = str(containers[target_index])
    target_string_trimmed = target_string[head_trim:tail_trim]
    data = json.loads(target_string_trimmed)
    # Return total pages
    total_results = round(int(espc_total_pages(data)/50))
    return total_results

def result_append(orig_db, new_db):
    df_orig_db = fetch.mock_input(orig_db)
    df_new_db = fetch.mock_input(new_db)
    frames = [df_orig_db, df_new_db]
    result = pd.concat(frames)
    result.drop_duplicates(inplace=True, keep="last", subset=['url'])
    fetch.save_to_csv(result,'merged_')
    return result

def espc_crawl():
    url = "https://espc.com/house-prices/" + str(area) + "?ps=50&p=" + str(p_pagenum) + "&sold=" + str(p_period)
    #Calculate pages
    page = page_calc_espc(url)
    print("Total pages to fetch: " + str(page))
    df_espc_cum = pd.DataFrame()
    for x in range(page):
        print("Fetching Page:" + str(x + 1))
        url = "https://espc.com/house-prices/" + str(area) + "?ps=50&p=" + str(x+1) + "&sold=" + str(p_period)
        print("Currently fetching " + url)


        target_index = -1
        response = get(url)
        html_soup = BeautifulSoup(response.text, 'html.parser')
        # containers = html_soup.find('script', type='application/json')
        containers = html_soup.find_all('script')
        for i in range(len(containers)):
            if (str(containers[i]).find('postcode')) != -1:
                target_index = i
                # TODO: Need error handling here.
        # print(containers[target_index])
        # Remove first few characters for JSON
        head_trim = str(containers[target_index]).find('{')
        tail_trim = str(containers[target_index]).rfind(';')
        target_string = str(containers[target_index])
        target_string_trimmed = target_string[head_trim:tail_trim]
        data = json.loads(target_string_trimmed)

        # Extract address from Columns
        df_espc_initial = pd.DataFrame(data['properties'])
        address_extract_lst = df_espc_initial['address'].tolist()
        df_espc_interim = pd.concat([df_espc_initial.drop(['address'], axis=1), pd.DataFrame(address_extract_lst)], axis=1)
        # Extract price information from Columns
        price_extract_lst = df_espc_interim['saleHistory'].tolist()
        df_espc_i2 = pd.concat([df_espc_interim.drop(['saleHistory'], axis=1), pd.DataFrame(price_extract_lst)], axis=1)

        # Convert to string to avoid '0' column
        df_espc_i2.columns = df_espc_i2.columns.map(str)
        df_espc_i2 = df_espc_i2.rename(index=str, columns={"0": "sale_1"})
        latest_price_extract_lst = df_espc_i2['sale_1'].tolist()
        df_espc_i2 = df_espc_i2.drop(['sale_1'], axis=1)
        # Revert index to int for merging
        df_espc_i2 = df_espc_i2.reset_index()
        df_price_to_concat = pd.DataFrame(latest_price_extract_lst)
        df_price_to_concat = df_price_to_concat.drop(['percentageIncrease'],axis=1)
        df_espc = pd.concat([df_espc_i2, df_price_to_concat], axis=1)
        # Rename Column name to match zoopla
        df_espc = df_espc.rename(index=str, columns={"bedrooms":"num_bedrooms","receptionRooms":"num_recepts", "lat":"latitude", "lon":"longitude","sellingPrice": "price", "displayAddress": "displayable_address"})
        df_espc_cum = df_espc_cum.append(df_espc, ignore_index=True)

    df_espc_cum.to_csv("espc_db.csv")

    return df_espc_cum

if __name__ == '__main__':
    # espc_crawl()
    result_append('espc_db_old.csv', 'espc_db.csv')