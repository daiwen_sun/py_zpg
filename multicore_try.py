import time
import multiprocessing
import fetch
import urllib.request
import pandas as pd
import numpy as np
import os
import json
import requests
import gmplot
import datetime
import matplotlib.pyplot as plt
import mplleaflet
import random
from geopy import distance
import numpy as np
import warnings
from difflib import SequenceMatcher
import os
import pickle


def info(title):
    print(title)
    print('module name:', __name__)
    print('parent process:', os.getppid())
    print('process id:', os.getpid())


def df_slicing(df_rent, n_proc):
    query_lst = []
    t_lst = []
    total_rows = int(len(df_rent.index))
    s_slice = round(total_rows / n_proc)
    # TODO: Bug here, need to cope with 1st input being '1' instead of '0'
    for t in range(0, total_rows, s_slice):
        t_lst.append(t)
    # Add last number for query
    t_lst.append(total_rows)

    # Building query here
    for j in range(len(t_lst) - 1):
        query_str = str(t_lst[j] + 1) + ' <= index <= ' + str(t_lst[j + 1])
        query_lst.append(query_str)

    return query_lst


def multi_proc(df_rent, df_sold, n_proc, radius, n_default, n_matching_ratio):
    # TODO: Automatic multiprocess assignment support
    query_lst = df_slicing(df_rent, n_proc)
    print(query_lst)

    if (n_proc == 2):
        df_rent_s0 = df_rent.query(query_lst[0])
        df_rent_s1 = df_rent.query(query_lst[1])
        p1 = multiprocessing.Process(target=fetch.calc_r_vs_b_expand_multiprocess,
                                     args=(df_sold, df_rent_s0, radius, n_default, n_matching_ratio, 0, n_proc,))
        p2 = multiprocessing.Process(target=fetch.calc_r_vs_b_expand_multiprocess,
                                     args=(df_sold, df_rent_s1, radius, n_default, n_matching_ratio, 1, n_proc,))

        p1.start()
        p2.start()

        p1.join()
        p2.join()
    elif (n_proc == 1):
        fetch.calc_r_vs_b_expand_multiprocess(df_sold, df_rent, radius, n_default, n_matching_ratio, 0, n_proc)
    else:
        df_rent_s0 = df_rent.query(query_lst[0])
        df_rent_s1 = df_rent.query(query_lst[1])
        df_rent_s2 = df_rent.query(query_lst[2])
        df_rent_s3 = df_rent.query(query_lst[3])
        p1 = multiprocessing.Process(target=fetch.calc_r_vs_b_expand_multiprocess,
                                     args=(df_sold, df_rent_s0, radius, n_default, n_matching_ratio, 0, n_proc,))
        p2 = multiprocessing.Process(target=fetch.calc_r_vs_b_expand_multiprocess,
                                     args=(df_sold, df_rent_s1, radius, n_default, n_matching_ratio, 1, n_proc,))
        p3 = multiprocessing.Process(target=fetch.calc_r_vs_b_expand_multiprocess,
                                     args=(df_sold, df_rent_s2, radius, n_default, n_matching_ratio, 2, n_proc,))
        p4 = multiprocessing.Process(target=fetch.calc_r_vs_b_expand_multiprocess,
                                     args=(df_sold, df_rent_s3, radius, n_default, n_matching_ratio, 3, n_proc,))

        p1.start()
        p2.start()
        p3.start()
        p4.start()

        p1.join()
        p2.join()
        p3.join()
        p4.join()
    return 0


def dataframe_rebuild(n_proc):
    df_rebuilt = pd.DataFrame()
    # Read pickle files
    if (n_proc == 2):
        df_reb_0 = pd.read_pickle("processed_rvsb_seg0.pickle")
        df_reb_1 = pd.read_pickle("processed_rvsb_seg1.pickle")
        df_rebuilt = df_rebuilt.append(df_reb_0)
        df_rebuilt = df_rebuilt.append(df_reb_1, ignore_index=True)
    else:
        df_reb_0 = pd.read_pickle("processed_rvsb_seg0.pickle")
        df_reb_1 = pd.read_pickle("processed_rvsb_seg1.pickle")
        df_reb_2 = pd.read_pickle("processed_rvsb_seg2.pickle")
        df_reb_3 = pd.read_pickle("processed_rvsb_seg3.pickle")
        df_rebuilt = df_rebuilt.append(df_reb_0)
        df_rebuilt = df_rebuilt.append(df_reb_1)
        df_rebuilt = df_rebuilt.append(df_reb_2)
        df_rebuilt = df_rebuilt.append(df_reb_3, ignore_index=True)
    # Append them together

    return df_rebuilt


if __name__ == "__main__":
    t = time.time()
    n_proc = 4
    info('Process Start, running on ' + str(n_proc) + ' cores')
    df_sold = fetch.mock_input("espc_db.csv")
    df_return = fetch.mock_input("rent_db.csv")
    multi_proc(df_return, df_sold, n_proc, 150, 5, 0.9)
    df_incl_r_vs_b = dataframe_rebuild(n_proc)
    fetch.save_to_csv(df_incl_r_vs_b, "output_")
    print("Process done in : ", time.time() - t)
