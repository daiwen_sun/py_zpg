import urllib.request
import pandas as pd
import numpy as np
import os
import json
import requests
import gmplot
import datetime
import matplotlib.pyplot as plt
import mplleaflet
from urllib.parse import quote

api_key = "tgkmwyu33s5va3qvk39w47b6"
p_area = "Edinburgh"
p_postcode = "EH112HG"
p_max_price = "300"
p_outcode = "Springvalley Terrace, Morningside, Edinburgh EH10"
p_outcode = "EH1"
page = 1
p_pagenumber = str(page)
url_old = "http://api.zoopla.co.uk/api/v1/average_sold_prices.js?" + "&area_type=outcodes&output_type=county" + "&postcode=" + p_outcode + "&page_number=" + p_pagenumber + "&api_key=" + api_key
url = "https://api.zoopla.co.uk/api/v1/average_area_sold_price.js?api_key=" + api_key + "&area=" + p_outcode + "&output_type=outcode"
def get_avg_price():
    df_sold_price_all = pd.DataFrame()
    for x in range (1,40):
        p_outcode = "EH" + str(x)
        print("Current Fetching EH" + str(x))
        url = "https://api.zoopla.co.uk/api/v1/average_area_sold_price.js?api_key=" + api_key + "&area=" + p_outcode + "&output_type=outcode"
        print(url)
        contents = urllib.request.urlopen(url).read()
        data = json.loads(contents.decode('utf-8'))
        df_sold_price = pd.DataFrame(data)
        df_sold_price_all = df_sold_price_all.append(df_sold_price)
    df_sold_price_all = df_sold_price_all.drop_duplicates(subset=['average_sold_price_1year'], keep='first')
    save_to_csv(df_sold_price_all)
    return 0

def get_specific_property_price(df):
    data_cum = []
    data = []
    for ix in df.index:
        # Build url for enquiry using 'displayable address'
        p_outcode = quote(str(df.loc[ix]['displayable_address']))
        url = "https://api.zoopla.co.uk/api/v1/average_area_sold_price.js?api_key=" + api_key + "&area=" + p_outcode + "&output_type=outcode"
        contents = urllib.request.urlopen(url).read()
        data = json.loads(contents.decode('utf-8'))
        print(data)
        data_cum.append(data)
    print(data_cum)
    df_sold_price_result = pd.DataFrame(data_cum)
    df_sold_price_result.reset_index(drop=True, inplace=True)
    return df_sold_price_result

def save_to_csv(df):
    now = datetime.datetime.now()
    df.to_csv("sold_price_all_" + now.strftime("%d-%m-%Y-%H-%M") + ".csv")
    return 0

def geoprice_est(df_sold,df_rent):

    return df_rent

def main():
    # get_avg_price()
    df = pd.read_csv("test_input.csv")
    # df_sold_price = get_specific_property_price(df)
    # print(df_sold_price)
    # save_to_csv(df_sold_price)

main()