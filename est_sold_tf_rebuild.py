from sklearn.base import BaseEstimator, TransformerMixin, RegressorMixin, clone
from sklearn.linear_model import ElasticNet, Lasso,  BayesianRidge, LassoLarsIC
from sklearn.ensemble import RandomForestRegressor,  GradientBoostingRegressor
from sklearn.model_selection import KFold, cross_val_score, train_test_split
from sklearn.metrics import classification_report
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import RobustScaler
from sklearn.metrics import confusion_matrix
from sklearn.kernel_ridge import KernelRidge
from sklearn.pipeline import make_pipeline
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
import scipy.stats as stats
import lightgbm as lgb
import seaborn as sns
import xgboost as xgb
import pandas as pd
import numpy as np
import matplotlib
import warnings
import sklearn
import scipy
import json
import sys
import csv
import os
import fetch

def data_precond():
    train = fetch.mock_input('espc_db.csv')
    test = fetch.mock_input('rent_db.csv')
    print(type(train))
    print(type(test))
    print(train.shape)
    print(test.shape)
    print(train.info())
    print(test.info())
    print(train['price'].describe())
    print(test.describe())

    sns.set(rc={'figure.figsize': (9, 7)})
    sns.distplot(train['price'])
    # plt.show()
    # skewness and kurtosis
    print("Skewness: %f" % train['price'].skew())
    print("Kurtosis: %f" % train['price'].kurt())

    train.hist(figsize=(15, 20))
    plt.figure()
    plt.show()

    return train, test

if __name__ == '__main__':
    # print('matplotlib: {}'.format(matplotlib.__version__))
    # print('sklearn: {}'.format(sklearn.__version__))
    # print('scipy: {}'.format(scipy.__version__))
    # print('seaborn: {}'.format(sns.__version__))
    # print('pandas: {}'.format(pd.__version__))
    # print('numpy: {}'.format(np.__version__))
    # print('Python: {}'.format(sys.version))
    data_precond()

