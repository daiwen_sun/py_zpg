import urllib.request
import pandas as pd
import numpy as np
import os
import json
import requests
import gmplot
import datetime
import matplotlib.pyplot as plt
import mplleaflet
import random
from geopy import distance
import numpy as np
import warnings
from difflib import SequenceMatcher



def api_key_gen():
    df = pd.read_csv("api_key.csv")
    total_rows = len(df.index)
    api_key_sel = df.loc[random.randint(1, total_rows)]['api_key']
    print("API Key Selected: " + api_key_sel)
    return api_key_sel


# api_key = api_key_gen()
api_key = "tgkmwyu33s5va3qvk39w47b6"
p_area = "Edinburgh"
p_postcode = "EH112HG"
p_radius = "0.2"
p_listing_status = "rent"
p_max_price = "300"
page = 1
p_pagenumber = str(page)
url = "https://api.zoopla.co.uk/api/v1/property_listings.js?" + "&area=" + p_area + "&radius=" + p_radius + "&listing_status=" + p_listing_status + "&maximum_price=" + p_max_price + "&page_number=" + p_pagenumber + "&include_rented=1&page_size=100" + "&api_key=" + api_key


# def map_plot_matplot(df_input):
#     #New Map plotter
#     # plt.scatter(df_input['longitude'], df_input['latitude'], s = 30, c = '#00FF80')
#     plt.scatter(df_input['longitude'], df_input['latitude'], c='#FF0080', s=20)
#     for index,row in df_input.iterrows():
#         if int(row['price']) <= to_pw(600):
#             plt.scatter(df_input['longitude'], df_input['latitude'] , c = '#0000FF', s=20)
#         if int(row['price']) > to_pw(600) and int(row['price']) <= to_pw(650):
#             plt.scatter(df_input['longitude'], df_input['latitude'] , c = '#003FDF', s=20)
#         if int(row['price']) > to_pw(650) and int(row['price']) <= to_pw(700):
#             plt.scatter(df_input['longitude'], df_input['latitude'] , c = '#007FBF', s=20)
#         if int(row['price']) > to_pw(700) and int(row['price']) <= to_pw(750):
#             plt.scatter(df_input['longitude'], df_input['latitude'] , c = '#00BF9F', s=20)
#         if int(row['price']) > to_pw(750) and int(row['price']) <= to_pw(800):
#             plt.scatter(df_input['longitude'], df_input['latitude'], c = '#00FF80', s=20)
#         if int(row['price']) > to_pw(800) and int(row['price']) <= to_pw(900):
#             plt.scatter(df_input['longitude'], df_input['latitude'], c = '#55AA80', s=20)
#         if int(row['price']) > to_pw(900) and int(row['price']) <= to_pw(1000):
#             plt.scatter(df_input['longitude'], df_input['latitude'], c = '#AA5580', s=20)
#         if int(row['price']) > to_pw(1000):
#             plt.scatter(df_input['longitude'], df_input['latitude'], c = '#FF0080', s=20)
#     plt.show()
#     return 0

def deposit_calc(sale_price):
    return 0

def to_pw(pcm):
    price_pw = float(pcm) / 52 * 12
    return price_pw


def to_pcm(pw):
    price_pcm = float(pw) * 52 / 12
    return price_pcm


def avg_price():
    # TODO: Calculate average sold price and rent price
    df = pd.read_csv("sold_price_all_04-10-2018-15-00.csv")
    df = df.drop(columns=['Unnamed: 0'])
    return df


def calc_r_vs_b(df_input):
    # This is to add the ratio of Buy vs Rent
    df_sold_price = avg_price()

    for ix in df_input.index:
        df_postcode = str(df_input.loc[ix]['outcode'])
        extract_str = '(' + df_postcode + ')'
        # print(extract_str)
        df_strmatch = df_sold_price['postcode'].str.extract(extract_str, expand=False)
        df_strmatch = df_strmatch.dropna()
        strmatch_lst = df_strmatch.index.tolist()
        if not strmatch_lst:
            print('Postcode Not Included!')
            b_vs_r = 0
        else:
            area_sold_price = df_sold_price.loc[int(strmatch_lst[0])]['average_sold_price_3year']
            # print(df_sold_price.loc[int(strmatch_lst[0])]['average_sold_price_3year'])
            b_vs_r = area_sold_price / to_pcm(df_input.loc[ix]['price'])
        df_input.loc[ix, 'b_vs_r'] = b_vs_r
        df_input.loc[ix, 'area_sold_price'] = area_sold_price
    return df_input


def calc_r_vs_b_expand(df_sold, df_rent, radius, n_default, n_matching_ratio):
    # Distance lat,long
    rent_dst = []
    for ix in df_rent.index:
        k = 0
        total_addr_match_price = 0
        # List for the final distance and index in 'for sale'
        est_price_lst = []
        addr_similarity_lst = []
        num_bedroom_rent = df_rent.loc[ix]['num_bedrooms']
        df_rent_lat = float(df_rent.loc[ix]['latitude'])
        df_rent_lon = float(df_rent.loc[ix]['longitude'])
        rent_loc = (df_rent_lat, df_rent_lon)
        rent_addr = str(df_rent.loc[ix]['displayable_address'])

        # Iterating through sold data for matching
        for yx in df_sold.index:
            df_sold_lat = float(df_sold.loc[yx]['latitude'])
            df_sold_lon = float(df_sold.loc[yx]['longitude'])
            sale_loc = (df_sold_lat, df_sold_lon)
            sale_addr = str(df_sold.loc[yx]['displayable_address'])
            rent_dst.append((distance.distance(rent_loc, sale_loc).meters, yx))
            addr_similarity_lst.append((SequenceMatcher(None, rent_addr,sale_addr).ratio(),yx))
        rent_dst.sort()
        addr_similarity_trimmed = [p for p in addr_similarity_lst if p[0] > n_matching_ratio]
        addr_similarity_trimmed.sort(reverse=True)
        if addr_similarity_trimmed:
            print(df_sold.loc[addr_similarity_trimmed[0][1]]['displayable_address'], addr_similarity_trimmed[0][0])
            if num_bedroom_rent == int(df_sold.loc[addr_similarity_trimmed[0][1]]['num_bedrooms']):
                print("Bed match, Use this price as estimated price")
                # TODO: Perhaps to average all matching bed prices
                est_price = df_sold.loc[addr_similarity_trimmed[0][1]]['price']
                k = 300
            else:
                for p in range(len(addr_similarity_trimmed)):
                    print(df_sold.loc[addr_similarity_trimmed[p][1]]['price'])
                    total_addr_match_price = total_addr_match_price + df_sold.loc[addr_similarity_trimmed[p][1]]['price']
                est_price = total_addr_match_price/len(addr_similarity_trimmed)
                print("Average Price from address match: " + str(est_price))
                k = 200
        else:
            # Trim the result list into radius bounded output - TODO: Need to confirm if all 0 are invalid
            # TODO: Exception Handling when all the distance is more than radius limit
            # TODO: displayble address matching
            rent_dst_trimmed = [i for i in rent_dst if (i[0] > 1 and i[0] < radius)]
            if not rent_dst_trimmed:
                print("No property for sold found in the radius " + str(radius) + " m given")
                print("Setting to closest property sold price at " + str(int(rent_dst[0][0])) + " m")
                #If no property found in the range given, averaging the nearest n_default properties (as per n_default specifies)
                for l in range(n_default):
                    if df_sold.loc[rent_dst[l][1]]['num_bedrooms'] == num_bedroom_rent:
                        est_price_lst.append(df_sold.loc[rent_dst[l][1]]['price'])
            # min_dist = rent_dst_trimmed[0]
            else:
                for k in range(len(rent_dst_trimmed)):
                    est_price_lst.append(df_sold.loc[rent_dst_trimmed[k][1]]['price'])

            try:
                est_price = sum(est_price_lst)/len(est_price_lst)
            except ZeroDivisionError:
                warnings.warn("Divide by 0 detected, setting est_price to invalid")
                est_price = 100000000
            # TODO: Need to confirm not parking space
        b_vs_r = est_price / to_pcm(df_rent.loc[ix]['price'])
        df_rent.loc[ix, 'confidence'] = k
        df_rent.loc[ix, 'est_area_price'] = est_price
        df_rent.loc[ix, 'b_vs_r'] = b_vs_r
        # print(df_rent)
        print(df_rent.loc[ix])
        rent_dst.clear()
    return df_rent

def calc_r_vs_b_expand_multiprocess(df_sold, df_rent, radius, n_default, n_matching_ratio, segment, n_proc):
    # Distance lat,long
    rent_dst = []
    for ix in df_rent.index:
        k = 0
        total_addr_match_price = 0
        # List for the final distance and index in 'for sale'
        est_price_lst = []
        addr_similarity_lst = []
        num_bedroom_rent = df_rent.loc[ix]['num_bedrooms']
        df_rent_lat = float(df_rent.loc[ix]['latitude'])
        df_rent_lon = float(df_rent.loc[ix]['longitude'])
        rent_loc = (df_rent_lat, df_rent_lon)
        rent_addr = str(df_rent.loc[ix]['displayable_address'])

        # Iterating through sold data for matching
        for yx in df_sold.index:
            df_sold_lat = float(df_sold.loc[yx]['latitude'])
            df_sold_lon = float(df_sold.loc[yx]['longitude'])
            sale_loc = (df_sold_lat, df_sold_lon)
            sale_addr = str(df_sold.loc[yx]['displayable_address'])
            rent_dst.append((distance.distance(rent_loc, sale_loc).meters, yx))
            addr_similarity_lst.append((SequenceMatcher(None, rent_addr,sale_addr).ratio(),yx))
        rent_dst.sort()
        addr_similarity_trimmed = [p for p in addr_similarity_lst if p[0] > n_matching_ratio]
        addr_similarity_trimmed.sort(reverse=True)
        if addr_similarity_trimmed:
            print(df_sold.loc[addr_similarity_trimmed[0][1]]['displayable_address'], addr_similarity_trimmed[0][0])
            if num_bedroom_rent == int(df_sold.loc[addr_similarity_trimmed[0][1]]['num_bedrooms']):
                print("Bed match, Use this price as estimated price")
                # TODO: Perhaps to average all matching bed prices
                est_price = df_sold.loc[addr_similarity_trimmed[0][1]]['price']
                k = 300
            else:
                for p in range(len(addr_similarity_trimmed)):
                    print(df_sold.loc[addr_similarity_trimmed[p][1]]['price'])
                    total_addr_match_price = total_addr_match_price + df_sold.loc[addr_similarity_trimmed[p][1]]['price']
                est_price = total_addr_match_price/len(addr_similarity_trimmed)
                print("Average Price from address match: " + str(est_price))
                k = 200
        else:
            # Trim the result list into radius bounded output - TODO: Need to confirm if all 0 are invalid
            # TODO: Exception Handling when all the distance is more than radius limit
            # TODO: displayble address matching
            rent_dst_trimmed = [i for i in rent_dst if (i[0] > 1 and i[0] < radius)]
            if not rent_dst_trimmed:
                print("No property for sold found in the radius " + str(radius) + " m given")
                print("Setting to closest property sold price at " + str(int(rent_dst[0][0])) + " m")
                #If no property found in the range given, averaging the nearest n_default properties (as per n_default specifies)
                for l in range(n_default):
                    if df_sold.loc[rent_dst[l][1]]['num_bedrooms'] == num_bedroom_rent:
                        est_price_lst.append(df_sold.loc[rent_dst[l][1]]['price'])
            # min_dist = rent_dst_trimmed[0]
            else:
                for k in range(len(rent_dst_trimmed)):
                    est_price_lst.append(df_sold.loc[rent_dst_trimmed[k][1]]['price'])

            try:
                est_price = sum(est_price_lst)/len(est_price_lst)
            except ZeroDivisionError:
                warnings.warn("Divide by 0 detected, setting est_price to invalid")
                est_price = 100000000
            # TODO: Need to confirm not parking space
        b_vs_r = est_price / to_pcm(df_rent.loc[ix]['price'])
        df_rent.loc[ix, 'confidence'] = k
        df_rent.loc[ix, 'est_area_price'] = est_price
        df_rent.loc[ix, 'b_vs_r'] = b_vs_r
        # print(df_rent)
        # print(df_rent.loc[ix])
        rent_dst.clear()
        if n_proc == 1:
            continue
        else:
            df_rent.to_pickle("processed_rvsb_seg" + str(segment) + ".pickle")
    if n_proc == 1:
        return df_rent
    else:
        return 0

def get_all_pages(page):
    # Returns a DF for append
    df_selected_cont_cum = pd.DataFrame()
    for x in range(page):
        print("Fetching Page:" + str(x + 1))
        url = "https://api.zoopla.co.uk/api/v1/property_listings.js?" + "&area=" + p_area + "&radius=" + p_radius + "&listing_status=" + p_listing_status + "&maximum_price=" + p_max_price + "&page_number=" + str(
            x) + "&include_rented=1&page_size=100" + "&api_key=" + api_key
        contents_cont = urllib.request.urlopen(url).read()
        data_cont = json.loads(contents_cont.decode('utf-8'))
        # Taking the 'listing' from the json
        df_selected_cont = pd.DataFrame(data_cont["listing"])
        # Select certain fields in the 'listing'
        df_selected_cont = df_selected_cont[
            ['displayable_address', 'first_published_date', 'latitude', 'longitude', 'price', 'outcode',
             'short_description','num_bedrooms','num_recepts','num_bathrooms']]
        # Append previous result
        df_selected_cont_cum = df_selected_cont_cum.append(df_selected_cont, ignore_index=True)
    return df_selected_cont_cum

def gmap_plot_expand(df_input, heat_filename, column, nth0, nth1, nth2, nth3, nth4, nth5, nth6):
    # Plots a heatmap for overview
    gmap = gmplot.GoogleMapPlotter(55.948534, -3.200459, 16)
    gmap.heatmap(df_input['latitude'], df_input['longitude'])
    gmap.draw("Heatmap_overview.html")

    # Plots a scatter for detailed price
    gmap_price = gmplot.GoogleMapPlotter(55.948534, -3.200459, 16)
    for index, row in df_input.iterrows():
        # print(row[column], row['latitude'], row['longitude'])
        if int(row[column]) <= nth0:
            gmap_price.scatter((row['latitude'], row['latitude']), (row['longitude'], row['longitude']), '#0000FF',
                               size=20, marker=False)
        if int(row[column]) > nth0 and int(row[column]) <= nth1:
            gmap_price.scatter((row['latitude'], row['latitude']), (row['longitude'], row['longitude']), '#003FDF',
                               size=20, marker=False)
        if int(row[column]) > nth1 and int(row[column]) <= nth2:
            gmap_price.scatter((row['latitude'], row['latitude']), (row['longitude'], row['longitude']), '#007FBF',
                               size=20, marker=False)
        if int(row[column]) > nth2 and int(row[column]) <= nth3:
            gmap_price.scatter((row['latitude'], row['latitude']), (row['longitude'], row['longitude']), '#00BF9F',
                               size=20, marker=False)
        if int(row[column]) > nth3 and int(row[column]) <= nth4:
            gmap_price.scatter((row['latitude'], row['latitude']), (row['longitude'], row['longitude']), '#00FF80',
                               size=20, marker=False)
        if int(row[column]) > nth4 and int(row[column]) <= nth5:
            gmap_price.scatter((row['latitude'], row['latitude']), (row['longitude'], row['longitude']), '#55AA80',
                               size=20, marker=False)
        if int(row[column]) > nth5 and int(row[column]) <= nth6:
            gmap_price.scatter((row['latitude'], row['latitude']), (row['longitude'], row['longitude']), '#AA5580',
                               size=20, marker=False)
        if int(row[column]) > nth6:
            gmap_price.scatter((row['latitude'], row['latitude']), (row['longitude'], row['longitude']), '#FF0080',
                               size=20, marker=False)
    gmap_price.draw(heat_filename)


def page_calc(url):
    # Determines how many pages to fetch
    print("Calculating Pages...")
    print(url)
    contents = urllib.request.urlopen(url).read()
    data = json.loads(contents.decode('utf-8'))
    print("Total Results: " + str(data['result_count']))
    page = round(int(data['result_count'] / 100)) + 1
    print("Total pages to fetch:" + str(page))
    return page


def save_to_csv(df, prefix):
    now = datetime.datetime.now()
    df.to_csv('results/' + prefix + now.strftime("%d-%m-%Y-%H-%M") + ".csv")
    return 0


def mock_input(input_file_name):
    df = pd.read_csv(input_file_name, encoding='utf-8')
    df = df.drop(columns=['Unnamed: 0'])
    return df

def rvsb_plot_expand_mock():
    # df_return = mock_input("mock_input_07_10.csv")
    # # df_sold = pd.read_csv("forsale_db_master08-10-2018-16-00.csv")
    # df_sold = pd.read_csv("espc_db.csv")
    # df_sold = df_sold.drop(columns=['Unnamed: 0'])
    # df_incl_r_vs_b = calc_r_vs_b_expand(df_sold, df_return,300,5,0.9)
    df_incl_r_vs_b = pd.read_csv("output_09-10-2018-16-34.csv")
    print(df_incl_r_vs_b)
    gmap_plot_expand(df_incl_r_vs_b, "RVSB_scatter.html", 'b_vs_r', 140, 160, 180, 200, 250, 300, 350)
    save_to_csv(df_incl_r_vs_b,"output_")

def rvsb_plot_expand():
    page_to_fetch = page_calc(url)
    df_return = get_all_pages(page_to_fetch)
    save_to_csv(df_return, "rent_fetch_")
    df_sold = pd.read_csv("espc_db.csv")
    df_sold = df_sold.drop(columns=['Unnamed: 0'])
    df_incl_r_vs_b = calc_r_vs_b_expand(df_sold, df_return,150,5,0.9)
    print(df_incl_r_vs_b)
    gmap_plot_expand(df_incl_r_vs_b, "RVSB_scatter.html", 'b_vs_r', 140, 160, 180, 200, 250, 300, 350)
    save_to_csv(df_incl_r_vs_b,"output_")

def rent_update():
    page_to_fetch = page_calc(url)
    df_return = get_all_pages(page_to_fetch)
    df_db = mock_input('rent_db.csv')
    frames = [df_return, df_db]
    results = pd.concat(frames)
    results.drop_duplicates(inplace=True, keep="last", subset=['first_published_date'])
    save_to_csv(results, 'rent_fetch_')
    return results