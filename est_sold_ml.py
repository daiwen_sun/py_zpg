import sklearn
from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np
import fetch
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
from math import cos, sin
import matplotlib.pyplot as plt
import time
import re
from sklearn.model_selection import train_test_split
from sklearn import preprocessing, svm
from sklearn.linear_model import SGDRegressor, SGDClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.svm import SVR
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import r2_score

def lon_lat_norm(lon, lat):
    # Returns a tuple of x,y,z
    x = cos(lat) * cos(lon)
    y = cos(lat) * sin(lon)
    z = sin(lat)
    return (x,y,z)

def cord_norm(df_input):
    cord_lst = []
    cord_x = []
    cord_y = []
    cord_z = []
    lon_lst = df_input['longitude'].tolist()
    lat_lst = df_input['latitude'].tolist()
    for i in range(len(lon_lst)):
        cord_lst.append(lon_lat_norm(lon_lst[i], lat_lst[i]))
    for j in range(len(cord_lst)):
        cord_x.append(cord_lst[j][0])
        cord_y.append(cord_lst[j][1])
        cord_z.append(cord_lst[j][2])
    return cord_x, cord_y, cord_z

def postcode_dict_norm(df_input, db_flag):
    post_code_lst = []
    # Cope with ESPC vs Zoopla mismatch
    if db_flag == 'sale':
        postcode = df_input['postcode'].tolist()
    else:
        postcode = df_input['outcode'].tolist()
    for i in range(len(postcode)):
        pstcde_split = postcode[i].split()
        match = re.match(r"([a-z]+)([0-9]+)", pstcde_split[0], re.I)
        if match:
            int_ext = match.groups()
            post_code_lst.append(int_ext[1])
        else:
            post_code_lst.append(-1)
    post_code_lst = list(map(float,post_code_lst))

    # Normalise data by dividing 20 (Maximum EH20 in dataset)
    post_code_lst[:] = [x / 20 for x in post_code_lst]
    return post_code_lst

def bed_norm(df_input):
    beds_lst = []
    n_beds = df_input['num_bedrooms'].tolist()
    for i in range(len(n_beds)):
        # Adding 1 for any 0 bed flats
        if n_beds[i] == 0:
            n_beds[i] = 1
        beds_lst.append(n_beds[i])
    beds_lst = list(map(float,beds_lst))
    # Normalise data by dividing 20 (Maximum 5 beds in dataset)
    beds_lst[:] = [x / 5 for x in beds_lst]
    return beds_lst

def get_outliers(df_input, outliers_fraction=0.25):
    clf = svm.OneClassSVM(nu=0.95 * outliers_fraction + 0.05, kernel="rbf", gamma=0.1)
    clf.fit(df_input)
    result = clf.predict(df_input)
    return result

def model_test(X_input_reg, prices_lst_reg):
    models = [LinearRegression(),
              RandomForestRegressor(n_estimators=100, max_features='sqrt'),
              KNeighborsRegressor(n_neighbors=6),
              SVR(kernel='linear'),
              LogisticRegression()]
    tmp = {}
    TestModels = pd.DataFrame()
    X_train, X_test, y_train, y_test = train_test_split(X_input_reg, prices_lst_reg, test_size=0.2, random_state=4)
    for model in models:
        # get model name
        m = str(model)
        tmp['Model'] = m[:m.index('(')]
        # fit model on training dataset
        model.fit(X_train, y_train)
        # predict prices for test dataset and calculate r^2
        tmp['R2_Price'] = r2_score(y_test, model.predict(X_test))
        # write obtained data
        TestModels = TestModels.append([tmp])
    print(tmp['R2_Price'])
    TestModels.set_index('Model', inplace=True)

    fig, axes = plt.subplots(ncols=1, figsize=(10, 4))
    TestModels.R2_Price.plot(ax=axes, kind='bar', title='R2_Price')
    plt.show()
    return 0

def ml_set_build(df_input, db_flag):

    prices_lst = df_input['price'].tolist()
    cord_x, cord_y, cord_z = cord_norm(df_input)
    postcode_lst = postcode_dict_norm(df_input, db_flag)
    beds_lst = bed_norm(df_input)
    X_input = []
    X_input_reg = []
    prices_lst_reg = []
    for i in range(len(cord_x)):
        X_input_elm = [cord_x[i], cord_y[i], cord_z[i], postcode_lst[i], beds_lst[i]]
        X_input.append(X_input_elm)

    # Skip outlier selection process
    if db_flag == 'sale':
        outliers = get_outliers(X_input,0.15)
        for j in range(len(outliers)):
            if(outliers[j] == 1):
                X_input_reg.append(X_input[j])
                prices_lst_reg.append(prices_lst[j])
        unique, counts = np.unique(outliers, return_counts=True)
        print('Outliers detected: ' + str(dict(zip(unique,counts))))
        print('Final samples selected: ' + str(len(X_input_reg)))
        return X_input_reg, prices_lst_reg
    else:
        print('Predicting data, not picking outliers.')
        return X_input, prices_lst



def sold_price_est_ml():
    df_sold = fetch.mock_input('espc_db_reformat.csv')
    X_input_reg, prices_lst_reg = ml_set_build(df_sold, 'sale')
    # model_test(X_input_reg,prices_lst_reg)

    X_train, X_test, y_train, y_test = train_test_split(X_input_reg, prices_lst_reg, test_size=0.2, random_state=4)
    # clf = svm.SVR(kernel='rbf', C=1, gamma='scale')
    clf = RandomForestRegressor(n_estimators=100, max_features='sqrt')
    clf.fit(X_train, y_train)
    confidence = r2_score(y_test, clf.predict(X_test))
    print('Model fitting R^2 score: ' + str(confidence))
    return clf

def apply_prediction(df_input, clf):
    X_input_reg, prices_lst_reg = ml_set_build(df_input, 'rent')
    pred_price_arr = clf.predict(X_input_reg)
    pred_price_lst = list(map(float, pred_price_arr))
    return pred_price_lst


def run_regression():
    t = time.time()
    clf = sold_price_est_ml()
    df_rent = fetch.mock_input('mock_input_07_10.csv')
    pred_price_lst = apply_prediction(df_rent, clf)
    se = pd.Series(pred_price_lst)
    df_rent['est_price_ml'] = se.values
    fetch.save_to_csv(df_rent,'ml_test_output_')
    print("Process done in : ",time.time()-t)