import urllib.request
import pandas as pd
import numpy as np
import os
import json
import requests
import gmplot
import datetime
import matplotlib.pyplot as plt
import mplleaflet
from urllib.parse import quote
import random
import fetch


api_key = "tgkmwyu33s5va3qvk39w47b6"
p_area = "Edinburgh"
p_postcode = "EH112HG"
p_radius = "0.2"
p_listing_status = "sale"
p_max_price = "600000"
p_max_beds = "2"
page = 8
p_pagenumber = str(page)
url = "https://api.zoopla.co.uk/api/v1/property_listings.js?" + "&area=" + p_area + "&radius=" + p_radius + "&listing_status=" + p_listing_status + "&maximum_price=" + p_max_price + "&maximum_beds=" + p_max_beds + "&page_number=" + p_pagenumber + "&include_sold=1&page_size=100" + "&api_key=" + api_key

def get_all_pages(page):
    # Returns a DF for append
    df_selected_cont_cum = pd.DataFrame()
    for x in range(page):
        print("Fetching Page:" + str(x + 1))
        url = "https://api.zoopla.co.uk/api/v1/property_listings.js?" + "&area=" + p_area + "&radius=" + p_radius + "&listing_status=" + p_listing_status + "&maximum_price=" + p_max_price + "&page_number=" + str(
            x) + "&include_sold=1&page_size=100" + "&api_key=" + api_key
        contents_cont = urllib.request.urlopen(url).read()
        data_cont = json.loads(contents_cont.decode('utf-8'))
        # Taking the 'listing' from the json
        df_selected_cont = pd.DataFrame(data_cont["listing"])
        # Select certain fields in the 'listing'
        df_selected_cont = df_selected_cont[
            ['displayable_address', 'first_published_date', 'latitude', 'longitude', 'price', 'outcode',
             'short_description','num_bedrooms','num_recepts','num_bathrooms','street_name','listing_id']]
        # Append previous result
        df_selected_cont_cum = df_selected_cont_cum.append(df_selected_cont, ignore_index=True)
    return df_selected_cont_cum

def sale_accum(df_database, df_new):
    # This function is for adding new entries for sold properties
    df_database = df_database.append(df_new, ignore_index=True)
    df_database.drop_duplicates(inplace=True,keep="last",subset=['listing_id'])
    return df_database

def main():
    page = fetch.page_calc(url)
    df_return = get_all_pages(page)
    df_db_return = sale_accum(fetch.mock_input("forsale_db_master.csv"),df_return)
    fetch.save_to_csv(df_db_return,"forsale_db_master")
    # df_return = fetch.mock_input("forsale_db_master08-10-2018-12-15.csv")
    # df_return.drop_duplicates(inplace=True,keep="last",subset=['listing_id'])
    # fetch.save_to_csv(df_return, "forsale_db_master")
    # fetch.gmap_plot_expand(df_return,"sold_price_scatter.html","price",100000,120000,140000,150000,160000,180000,200000)

main()