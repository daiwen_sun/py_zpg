import urllib.request
import pandas as pd
import numpy as np
import os
import json
import requests
from IPython.display import display, HTML

def main():
    contents = urllib.request.urlopen("https://api.nestoria.co.uk/api?encoding=json&pretty=1&action=search_listings&country=uk&listing_type=buy&place_name=brighton").read()
    #print(contents)
    df = pd.read_json(contents, typ = "frame", orient='index')
    data = json.loads(contents.decode('utf-8'))
    print(df)
    display(HTML(df.to_html()))
main()