from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import itertools

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pylab import rcParams
import matplotlib
import tensorflow as tf
import fetch
import os
# Stop AVX Warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

from sklearn.ensemble import IsolationForest

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import time

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings("ignore", category=FutureWarning)



def data_import(train_set,test_set):
    # tf.logging.set_verbosity(tf.logging.INFO)
    sess = tf.Session()

    train = fetch.mock_input(train_set)
    print('Shape of the train data with all features:', train.shape)
    train = train.select_dtypes(exclude=['object'])
    print("")
    print('Shape of the train data with numerical features:', train.shape)
    train.drop(['index'],axis = 1, inplace = True)
    train.fillna(0,inplace=True)

    test = fetch.mock_input(test_set)
    test = test.select_dtypes(exclude=['object'])
    ID = test.index
    list(test.columns)
    test.fillna(0,inplace=True)

    print("")
    print("List of features contained our dataset:",list(train.columns))
    return train, test

def outlier_removal(train_set):
    clf = IsolationForest(max_samples=100, random_state=42)
    clf.fit(train_set)
    y_noano = clf.predict(train_set)
    y_noano = pd.DataFrame(y_noano, columns=['Top'])
    y_noano[y_noano['Top'] == 1].index.values

    train = train_set.iloc[y_noano[y_noano['Top'] == 1].index.values]
    train.reset_index(drop=True, inplace=True)
    print("Number of Outliers:", y_noano[y_noano['Top'] == -1].shape[0])
    print("Number of rows without outliers:", train.shape[0])
    return train

def preprocessing_train(train_set, test_set):
    warnings.filterwarnings('ignore')

    col_train = list(train_set.columns)
    col_train_bis = list(train_set.columns)
    col_train_bis.remove('price')

    mat_train = np.matrix(train_set)
    mat_test = np.matrix(test_set)
    mat_new = np.matrix(train_set.drop('price', axis=1))
    # mat_y = np.array(train_set.price).reshape(round(float(train_set.shape[0])*0.9), 1)

    # prepro_y = MinMaxScaler()
    # prepro_y.fit(mat_y)

    prepro = MinMaxScaler()
    prepro.fit(mat_train)

    prepro_test = MinMaxScaler()
    prepro_test.fit(mat_new)

    train_set= pd.DataFrame(prepro.transform(mat_train), columns=col_train)
    test_set = pd.DataFrame(prepro_test.transform(mat_test), columns=col_train_bis)

    train_set.head()


    # List of features
    COLUMNS = col_train
    FEATURES = col_train_bis
    LABEL = "price"

    # Columns for tensorflow
    feature_cols = [tf.contrib.layers.real_valued_column(k) for k in FEATURES]

    # Training set and Prediction set with the features to predict
    training_set = train_set[COLUMNS]
    prediction_set = test_set.price

    # Train and Test
    x_train, x_test, y_train, y_test = train_test_split(training_set[FEATURES], prediction_set, test_size=0.33,
                                                        random_state=42)
    y_train = pd.DataFrame(y_train, columns=[LABEL])
    training_set = pd.DataFrame(x_train, columns=FEATURES).merge(y_train, left_index=True, right_index=True)
    training_set.head()

    # Training for submission
    training_sub = training_set[col_train]
    return 0

if __name__ == '__main__':
    train_set, test_set = data_import('espc_db.csv', 'mock_input_07_10.csv')
    train_set = outlier_removal(train_set)
    preprocessing_train(train_set, test_set)